import requests 
id_project = "curso_versiones_git%2Ftrabajando_local%2Frepo_grupal"
url = f"https://gitlab.com/api/v4/projects/{id_project}/issues"
headers = {"PRIVATE-TOKEN": "glpat-HAgLncyBDPnzTnGieMs_"}
data = {"title": "issue_samuel"}

response = requests.post(url, headers=headers, data=data)

if response.status_code == 201:
    new_issue = response.json()
    print(f"Nuevo Issue ID: {new_issue['id']}, Title: {new_issue['title']}")
else:
    print(f"Error: {response.status_code}, {response.text}")